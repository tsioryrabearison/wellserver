const express = require('express');
const http = require('http')
const app = express()
const bodyParser = require('body-parser');
const cors = require('cors');
const shell = require('shelljs');
const path = require('path')

const database = require('./server/route/db.controller.js')
const welldb = new database('wellcoloc')

app.use(cors())

// Parsers
app.use(bodyParser.json({ limit: '50Mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50Mb' }));

//static file
app.use('/ftp', express.static('ftp'))


const server = http.createServer(app)

//------------------------ SOCKET IO ----------------------- //
//Socket.Io
let io = require('socket.io')(server)

// API location
io.users = []
io.comments = []

const api = require('./server/route/route.js')(io)

app.use('/api/', async (req, res, next) => {

    if(!welldb.mongo)
        await welldb.connect()
    
    req.db = welldb
    req.io = io

    next()    
}, api)

app.get('/', (req, res) => {
    res.end('Wellcoloc server connected.')
})

app.post('/gitupdate', (req, res) => {
    console.log('[' + new Date() + '] GIT PULL .........................')
    shell.cd('/home/dev/www/wellserver/')
    shell.exec('git pull origin master')
    console.log('[' + new Date() + '] NPM INSTALL .............................')
    shell.exec('npm install --save')
    console.log('[' + new Date() + '] RESTARTING APP .............................')
    shell.exec('pm2 restart wellserver')
    console.log('[' + new Date() + '] Webhook bitbuckets Received and Updated...............')
    res.json({ response: 'webhook bitbuckets received successfully..........' })
})

//Set Port
const port = 4000;

app.set('port', port);

server.listen(port, () => console.log(`Running on localhost:${port}`));