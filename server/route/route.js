
const express = require('express')
const router = express.Router()

const crud = require('./api/crud')
const user = require('./api/user')
const message = require('./api/message')
const notification = require('./api/notification')
const feed = require('./api/feed')

router.use('/crud', crud)
router.use('/user', user)
router.use('/message', message)
router.use('/notification', notification)
router.use('/feed', feed)

module.exports = (io) => {

	io.on('connection', socket => {

	    io.isConnected = (_id_user, callback) => {
	        if(io.users){
	            let user = io.users.filter(u => String(u._id) === String(_id_user))
	            user.lenght === 0 ? callback(true): callback(false, user)
	        }
    	}

	    socket.on('logged-in', (_id_user) => {
	        console.log('new user === '+ _id_user +' === connected !', socket.id)
	        
	        // let isIdUserExist = false

	        io.users = io.users || []

	        // io.users.filter((user, index) => {
	        //     if(user._id === _id_user){
	        //         socket = user.socket
	        //         isIdUserExist = true
	        //     }
	        // })

	        // if(!isIdUserExist)
	            io.users.push({ _id: _id_user, socket: socket})
	        
	    })


	    socket.on('logged-out', (_id_user) => {
	        
	        io.users = io.users || []

	        io.users.filter((user, index) => {

	            if(user._id === _id_user && user.socket.id === socket.id){
	                io.users.splice(index, 1)
	            }
	        })
	        console.log('user <<< '+ _id_user +' <<< disconnected')
	    })

	    socket.on('updateChatNotifClient', (_id_client) => {
	    	io.users.filter(user => {
  	 			if(user._id === _id_client)
					io.to(user.socket.id)
      	 				.emit('updateChatNotifClient')
  	 		})
	    })

	    socket.on('updateAnnonceFeed', (_id_client) => {
	    	socket.broadcast.emit('socketUpdateAnnonceFeed')
	    	io.users.filter(user => {
  	 			if(user._id === _id_client)
	                io.to(user.socket.id).emit('updateAnnonceNotifClient')
  	 		})
	    })

	    socket.on('comments-enter', _id_annonce => {
	        socket.join('comments_'+ _id_annonce);
	    })

		socket.on('comments-leave', _id_annonce => {
	        socket.leave('comments_'+ _id_annonce);
	    })

	    socket.on('updateComment', _id_annonce => {
	    	io.sockets.in('comments_'+ _id_annonce).emit('updateComment')
	    })

	})

	return router
}