const express = require('express')
const router = express.Router()
const ObjectId = require('mongodb').ObjectID

router.get('/:_id_user/chat', (req, res) => {
	if(req.params._id_user.length === 24 || req.params._id_user.length === 12){
		const q = {
			'client._id': ObjectId(req.params._id_user)
		}

		req.db.setCollection('chat')
			.read(q)
			.then(data => { res.json({ success: true, body: data }) })
			.catch(() => { res.json({ success: false }) })
	}		
})

router.get('/chat', (req, res) => {

	const _id_chat = req.query._id_chat

	req.db.setCollection('chat')

	if(!_id_chat){
		req.db.read({
			$or:[{
				$and: [{
					"client.0._id" : ObjectId(req.query._id_client)
				}, {
					"client.1._id" : ObjectId(req.query._id_user)
				}]
			}, {
				$and: [{
					"client.0._id" : ObjectId(req.query._id_user)
				}, {
					"client.1._id" : ObjectId(req.query._id_client)
				}]
			}]
		}).then(data => {
			res.json({ success: true, body: data })

		}).catch(() => {

			if(req.query._id_client && req.query._id_user){
				req.db.save({
					client: [
						{ _id: ObjectId(req.query._id_client) },
						{ _id: ObjectId(req.query._id_user) }
						],
					messages: []

				})
				.then((results) => { res.json({ success: false, _id_chat: results.insertedId }) })
				.catch(e => { res.json({ success: false }) })

			}else{
				res.json({ success: false })
			}
		})
		
	} else{
		req.db
			.read({ _id: ObjectId(_id_chat) })
			.then(data => { res.json({ success: true, body: data }) })
			.catch(e => { res.json({ success: false }) })
	}
	
})

router.post('/delete', (req, res) => {
	req.db.setCollection('chat')
		.query({ _id: ObjectId(req.body._id_chat) })
		.delete()
		.then(() => {
			req.db.setCollection('user')
			   .query({ _id: ObjectId(req.body._id_client) })
			   .pull(`notification.chat`, { _id_user: ObjectId(req.body._id_user )} ) 
			   .then(() => {
			   		req.db.query({ _id: ObjectId(req.body._id_user) })
			   		   .pull(`notification.chat`, { _id_user: ObjectId(req.body._id_client )} ) 
			   		   .then(() => res.json({ success: true }))
			   		   .catch(() => { res.json({ success: false }) })
			   })
			   .catch(() => { res.json({ success: false }) })
		})
		.catch(() => { res.json({ success: false }) })
})

router.post('/send', (req, res) => {


	const _id_chat = req.body._id_chat

	let value = {
		_id_sender: ObjectId(req.body._id_sender),
		date_send: new Date(),
		text: req.body.text
	}
	
	req.db.setCollection('chat')
		.query({ _id: ObjectId(_id_chat) })
		.read()
		.then(messages => {

			let _id_client

			if(String(messages[0].client[0]._id) === String(value._id_sender)){
				_id_client = messages[0].client[1]._id
			
			}else
				_id_client = messages[0].client[0]._id

			req.db.push('messages', value)
				.then(data => { 
				/* update notif user */
	              req.db.setCollection('user')
	              	 .query({ _id: _id_client, "notification.chat._id_user": value._id_sender })
	              	 .read()
	              	 .then(() => {
	              	 	req.db.set('notification.chat.$.date_send', value.date_send)
	              	 })
	              	 .catch(() => {
	              	 	req.db.query({ _id: _id_client })
	              	 		.push('notification.chat', { 
	              	 			_id_user: value._id_sender, 
	              	 			date_send: value.date_send
	              	 	})
	              	 })

	              	 /* send socket to client */
	              	 req.io.isConnected(_id_client, (err, client) => {
						
						try {
							if (!err){
	              	 			req.io.users.filter(user => {

								console.log('ato====', user._id === _id_client)
								console.log('iito====', String(user._id) === String(_id_client))

	              	 			if(String(user._id) === String(_id_client)){
	              	 				console.log('===== SEND SOCKET MESSAGE TO ' + user._id)
									req.io.to(user.socket.id)
		              	 				.emit('socketUpdateChat', user.socket.id)
	              	 			}
		              	 		})
		              	 	}
		              	 	else{
		              	 		console.log('===== ERROR !!!!!', err)
		              	 	}
						} catch(error){
							console.log('====ERROR CATCHHH ', error)
						}


	              	 })
	              	/* -------------------- */

				res.json({ success: true, body: data })

			}).catch(() => { res.json({ success: false }) })
		
		}).catch(() => {
			res.json({ success: false })		
		})
})


module.exports = router