const express = require('express')
const router = express.Router()
const ObjectId = require('mongodb').ObjectID

router.get('/:_id_user', (req, res) => {
	//type = annonce - chat
	req.db.setCollection('annonce')
		.query({ _id_user: ObjectId(req.params._id_user) })
		.read()
		.then(data => {
			let body = []
			let reaction_annonce = []

			data.filter(annonce => {

				function populateReaction(type){
					annonce.reaction_annonce[type].filter(item => {
						reaction_annonce.push({  
							_id_annonce: annonce._id,
							_id_user: item._id_user, 
							type: type, 
							date_send: item.date_insertion
						})
					})
				}

				populateReaction('follow')
				populateReaction('like')
				populateReaction('star')
				populateReaction('comment')

				body.push(reaction_annonce.sort((v1, v2) => new Date(v2.date_send) - new Date(v1.date_send)))
			})

			res.json({ success: true, body: body })
		})
		.catch(() => res.json({ success: false }))

})

router.get('/:_id_user/counter?', (req, res) => {
	
	req.db.setCollection('user')
		.query({ _id: ObjectId(req.params._id_user) })
		.read()
		.then(data => {
			res.json({ success:true, body: data[0].notification[req.query.type].length })
		})
		.catch(() => res.json({ success: false }))
})

router.post('/delete', (req, res) => {


	if(req.body.type === 'chat'){
		
		req.db.setCollection('user')
		   .query({ _id: ObjectId(req.body._id ) })
		   .pull(`notification.chat`, {_id_user: ObjectId(req.body._id_client )} )
		   .then(() => {
		   		req.db.read()
		   			.then(body => {
		   				res.json({ success: true, body: body }) 
		   			})
		   })
		   .catch(() => { res.json({ success: false }) })
	}else{
		req.db.setCollection('user')
		   .query({ _id: ObjectId(req.body._id )})
		   .set(`notification.annonce`, [])
		   .then(() => { res.json({ success: true }) })
		   .catch(() => { res.json({ success: false }) })
	}
})

module.exports = router