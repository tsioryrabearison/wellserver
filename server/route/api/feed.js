const express = require('express')
const router = express.Router()
const ObjectId = require('mongodb').ObjectID

router.post('/updateReaction', (req, res) => {

	req.db.setCollection('annonce')

	const q = { _id: ObjectId(req.body._id_annonce) }

	let value = {
    "_id_client": ObjectId(req.body.value._id_client),
		"_id_user": ObjectId(req.body.value._id_user),
    "date_insertion": new Date()
	}

	switch(req.body.reaction){

      case "follow":
      case "like":

      	const type = `reaction_annonce.${req.body.reaction}`

        req.db.read({ 
              _id: ObjectId(req.body._id_annonce),
              [type]: { $elemMatch: { _id_user: value._id_user } } 
            })
            .then(() => {
              req.db.query(q)
                .pull(type, { _id_user: value._id_user })
                .then(() => res.json({ success: true })) 
                .catch(e => { res.json({ success: false }) }) 

              /* update notif user */
              req.db.setCollection('user')
                 .query({ _id: ObjectId(value._id_client)})
                 .pull('notification.annonce', { _id_user: value._id_client, type: req.body.reaction })
              /* -------------------- */       
            })
            .catch(() => {
              req.db.query(q)
                .push(type, value)
                .then(() => res.json({ success: true }))
                .catch(e => { res.json({ success: false }) }) 

              /* update notif user */
              req.db.setCollection('user')
                 .query({ _id: ObjectId(value._id_client)})
                 .push('notification.annonce', { _id_user: value._id_client, type: req.body.reaction })
              /* -------------------- */

              
            })

        break

      case "star":

      	value.value = req.body.value.rating || 0

        req.db.read({ 
             'reaction_annonce.star': { $elemMatch: { _id_user: ObjectId(req.body.value._id_user) } } 
            })
            .then(() => {
              req.db.query(q)
                .pull("reaction_annonce.star", { _id_user: value._id_user })
                .then(() => {
                  req.db
                    .push('reaction_annonce.star', value)
                    .then(() => res.json({ success: true }))
                    .catch(e => { res.json({ success: false }) }) 

                })

            })
            .catch(() => {
              req.db.query(q)
                .push('reaction_annonce.star', value)
                .then(() => res.json({ success: true }))
                .catch(e => { res.json({ success: false }) }) 

              /* update notif user */
              req.db.setCollection('user')
                 .query({ _id: ObjectId(value._id_client)})
                 .push('notification.annonce', { _id_user: value._id_client, type: 'star' })
              /* -------------------- */
            })
        
        break 

      case "status_annonce":
        req.db.read(q)
            .then(data => {
              value = data[0].reaction_annonce.available || false
              req.db.set('reaction_annonce.available', !value, q)
                .then(() => res.json({ success: true }))
                .catch(e => { res.json({ success: false }) }) 
          })
      	
      	break

      case "comment":
      	value.comment = req.body.value.comment
      	value._id_comment = ObjectId()

      	req.db.push('reaction_annonce.comment', value, q)
  	    	.then(() => res.json({ success: true }))
  		    .catch(() => res.json({ success: false }))

        /* update notif user */
          req.db.setCollection('user')
             .query({ _id: ObjectId(value._id_client)})
             .push('notification.annonce', { _id_user: value._id_client, type: 'comment' })
          /* -------------------- */

        break
    }
})

router.get('/getAllAnnonces', (req, res) => {
  req.db.setCollection('annonce')
    .read({})
    .then(annonces => {
      if(annonces){
        req.db.setCollection('user')
        .read({})
        .then(users => {
            annonces.map(annonce => annonce.user = users.filter(u => String(annonce._id_user) === String(u._id))[0])
            res.json({ success: true, body: annonces })
        })
      }else{
        res.json({ success: false })
      }
    })
})

router.get('/comments', (req, res) => {

  req.db.setCollection('annonce')
    .query({ _id: ObjectId(req.query._id_annonce) })
    .read()
    .then(annonce => {
      if(annonce){
        req.db.setCollection('user')
          .read({})
          .then(users => {
            comments = annonce[0].reaction_annonce.comment; 
            comments.map(comment => comment.user = users.filter(u => String(comment._id_user) === String(u._id))[0])
            res.json({ success: true, body: comments })
        })
      }else{
        res.json({ success: false })
      }
    })
})

router.post('/deleteComment', (req, res) => {

	const q = { _id: ObjectId(req.body._id_annonce) }

	req.db.setCollection('annonce')
		.pull('reaction_annonce.comment', { _id_comment: ObjectId(req.body._id_comment) }, q)
		.then(() => res.json({ success: true }))
		.catch(() => res.end({ success: false }))
})

router.post('/updateComment', (req, res) => {

	const q = { 
		_id: ObjectId(req.body._id_annonce),
		'reaction_annonce.comment._id_comment': ObjectId(req.body._id_comment )
	}

	const value = {
		'reaction_annonce.comment.$.comment': req.body.updateComment
	}
	
	req.db.setCollection('annonce')
		.update(value, q)
		.then(() => res.json({ success: true }))
		.catch(() => res.end({ success: false }))
})

module.exports = router