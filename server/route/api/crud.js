
const express = require('express')
const router = express.Router()
const fs = require('fs')
const mkdirp = require('mkdirp');
const ObjectId = require('mongodb').ObjectID
// const rootFolder = "https://server.wellcoloc.com"
// const rootFile = `${process.cwd()}/`
const rootFolder = "http://localhost:4000"
const rootFile = ``

router.post('/post', (req, res) => {

	console.log('REQUEST POST....')

	req.db.setCollection(req.body.collection)
		.read(req.body.query)
		.then(data => {
			res.json({ success: true, body: data })
		})
		.catch(err => {
			res.json({ success: false })
		})
})

router.get('/q', (req, res) => {
	
	console.log('REQUEST QUERY....')
	
	let collection = req.query.collection
	
	delete req.query.collection

	for(let key in req.query){
		if(key.match(/\b_id/g) && 
			(req.query[key].length === 12 || req.query[key].length === 24)
		){
			req.query[key] = ObjectId(req.query[key])
		}
	}

	req.db.setCollection(collection)
		.query(req.query)
		.read()
		.then(data => { res.json({ success: true, body: data }) })
		.catch(e => { res.json({ success: false }) })
})

router.get('/get', (req, res) => {
	
	let query = {}

	console.log('REQUEST GET....')

	if(req.query.ObjectId){
		query = { _id: ObjectId(req.query.ObjectId) }
	}

	if(req.query.collection){
		req.db.setCollection(req.query.collection)
			.read(query)
			.then(data => { res.json({ success: true, body: data }) })
			.catch(e => { res.json({ success: false }) })
	}

	if(req.query.count){
		req.db
			.getCollection(req.query.count)
			.countDocuments()
			.then(c => { res.json({ success: true, body: c }) })
			.catch(e => { res.json({ success: false }) })
	}

})

router.post('/update', (req, res) => {

	console.log('REQUEST UPDATE....')

	const q = { _id: ObjectId(req.body._id) }

	req.body.value.date_update = new Date()

	req.db.setCollection(req.body.collection)
		.update(req.body.value, q)
		.then(() => { res.json({ success: true }) })
		.catch(() => { res.json({ success: false }) })
})

router.post('/save', (req, res) => {

	console.log('REQUEST SAVE....')

	req.body.value.date_insertion = new Date()

	if(req.body.value._id_user)
		req.body.value._id_user = ObjectId(req.body.value._id_user)

	req.db.setCollection(req.body.collection)
		.save(req.body.value)
		.then((result) => { res.json({ success: true, _id: result.insertedId }) })
		.catch(() => { res.json({ success: false }) })
})

router.post('/delete', (req, res) => {

	console.log('REQUEST DELETE....')

	req.db.setCollection(req.body.collection)
		.delete({ _id: ObjectId(req.body._id) })
		.then(() => { res.json({ success: true }) })
		.catch(() => { res.json({ success: false }) })
})

router.post('/uploadFile',( req, res) => {

	let buf = new Buffer(req.body.blob, 'base64'); // decode
	
	mkdirp(`${rootFile}ftp/${req.body.folder}`, (err) => {
		writeFileOnFolder(req.body.folder, data => res.json(data))
	})

  function writeFileOnFolder(folder, callback){
  	fs.writeFile(`${rootFile}ftp/${folder}/${req.body.name}.jpg`, buf, err => {
      if (err){
          console.log("Error upload file " + req.body.name + ".jpg", err)
          callback({ success: false })
      }
      else {
      	console.log('file uploaded!')
		callback({ success: true, imgUrl: rootFolder + "/ftp/"+ folder +"/" + req.body.name + ".jpg"})
      }
	})
  }

})

module.exports = router