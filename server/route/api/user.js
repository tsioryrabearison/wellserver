const express = require('express')
const router = express.Router()
const ObjectId = require('mongodb').ObjectID

router.get('/:_id_user', (req, res) => {

	req.db.setCollection('user')
		.query({ _id: ObjectId(req.params._id_user) })
		.read()
		.then(async (data) => { 

			let annonce_user = await req.db.setCollection('annonce')
				.query({ _id_user: ObjectId(req.params._id_user) })
				.read()
				.then(res => res)
				.catch(e => null)

			let location = 0
			let colocation = 0

			if(annonce_user)
				annonce_user.filter(annonce => {
					return annonce.type_annonce === 'location' ? location++ : colocation++
				})

			req.db.setCollection('login')
				.read()
				.then(user => {
					data[0].pseudo = user[0].pseudo
					data[0].annonce = {
						location: location,
						colocation: colocation
					}
					
					res.json({ success: true, body: data })

				}).catch(e => { res.json({ success: false }) })

	}).catch(e => {
		res.json({ success: false })
	})
})

router.post('/profilePicture', (req, res) => {

	req.db.setCollection('user')
		.query({ _id: ObjectId(req.body._id_user) })
		.set('pic_prof', [req.body.imgUrl])
		.then(() => {
			res.json({  success: true })
		}).catch(e => { res.json({ success: false }) })
})

router.post('/login', (req, res) => {

	req.db.setCollection('login')
		.read({ $and: [{ pseudo: req.body.pseudo }, { password: req.body.password }] })
		.then(data => {
			req.db.setCollection('user')
				.set('status_user.connected', true, { _id: ObjectId(data[0]._id_user) })

			res.json({ success: true, _id_user: data[0]._id_user })

		}).catch(e => { res.json({ success: false }) })
})

router.post('/logout', (req, res) => {
	
	req.db.setCollection('user')
		.set('status_user.connected', false, { _id: ObjectId(req.body._id_user) })
		.then(() => { res.json({ success: true }) })
		.catch(e => { res.json({ success: false }) })
})

router.post('/delete', (req, res) => {

	let deleteChatUser = () => {
		return req.db.setCollection('chat')
			.query({ $or: [
				{ "client.0._id": ObjectId(req.body._id_user) },
				{ "client.1._id": ObjectId(req.body._id_user) }
			]})
			.delete()
	}

	let deleteLoginUser = () => {
		return req.db.setCollection('login')
			.query({ _id_user: ObjectId(req.body._id_user) })
			.delete()
	}

	let deleteAnnonce = () => {
		return req.db.setCollection('annonce')
			.query({ _id_user: ObjectId(req.body._id_user) })
			.delete()
	}

	let deleteUser = () => {
		return req.db.setCollection('user')
			.query({ _id: ObjectId(req.body._id_user) })
			.delete()
	}

	Promise.all([deleteChatUser, deleteLoginUser, deleteAnnonce, deleteUser])
		.then(() => {
			res.json({ success: true })
		}).catch(() => {
			res.json({ success: false })
		})
})

module.exports = router