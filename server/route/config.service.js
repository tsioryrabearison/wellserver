'use strict';

module.exports = class DB {

    constructor(host, port) {
        this.dbConnected = false;
        this.mongoDb = null;
        this.host = host;
        this.port = port;
    }

    connect(user, password) {
        console.log('Connecting to database '+ this.host +':'+ this.port +'...')
        let MongoClient = require('mongodb').MongoClient;
        let promise = new Promise((resolve, reject) => {
            if (!this.dbConnected) {

                let config = `mongodb://${this.host}:${this.port}`
                // let config = `mongodb://user@password:${this.host}:${this.port}`
                //                 .replace('<dbuser>', user)
                //                 .replace('<dbpassword>', password);
                MongoClient.connect(config, { useNewUrlParser: true }, (err, client) => {
                    if (err) {
                        console.log('Database connection failed !\n')
                        console.log(err)
                        reject(err)
                    }else{
                        console.log('Database connected.')
                        this.dbConnected = true;
                        // database check for mongod 3^
                        this.mongoDb = client;
                        // this.mongoDb = client.db('wellcoloc');
                        
                        resolve(this.mongoDb);
                    }
                })
            } else
                resolve(this.mongoDb)
        });

        return promise;
    }
}