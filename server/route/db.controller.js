const DB = require('./config.service')
const dbSetup = new DB("localhost", 27017)

module.exports = class {

    constructor(databaseName) {
        this.database = databaseName;
        this.collection;
        this.q = {};
        this.mongo;
    }

    setCollection(collection) {
        this.collection = collection
        return this
    }

    getCollection(collection){
        const c = collection || this.collection
        return this.mongo.db.collection(c)
    }

    query(q){
        this.q = q
        return this
    }

    async connect() {
        return this.mongo = !this.mongo ? await dbSetup.connect().then(db => {
            return { db: db.db(this.database), MongoClient: db}
        }) : this.mongo
    }

    async disconnect(callback) {
        if(this.mongo) {
            this.mongo.MongoClient.close(() => {
                console.log('database disconnected !')
                this.mongo = null;
                callback()
            })
        }
    }

    async read(query) {
        
        let q = query || this.q

        return new Promise((resolve, reject) => {
            this.mongo.db.collection(this.collection)
                .find(q)
                .sort({_id:-1}) 
                .toArray((err, body) => {
                    if (body ? body.length === 0 : err) {
                        reject(err)
                    } else {
                        resolve(body)
                    }
                })
        })
    }

    async delete(query) {

        let q = query || this.q

        return this.mongo.db.collection(this.collection)
            .deleteOne(q)
    }

    async save(value) {
        return this.mongo.db.collection(this.collection)
            .insertOne(value)
    }

    async update(value, query) {

        let q = query || this.q

        return this.mongo.db.collection(this.collection)
            .updateOne(query, {
                $set: value
            })
    }

    async set(key, value, query) {

        let q = query || this.q

        return this.mongo.db.collection(this.collection)
            .updateOne(q, {
                $set: {
                    [key]: value
                }
            })
    }

    async addToSet(key, value, query) {

        let q = query || this.q

        return this.mongo.db.collection(this.collection)
            .updateOne(q, {
                $addToSet: value
            })
    }

    async push(key, value, query) {

        let q = query || this.q

        return this.mongo.db.collection(this.collection)
            .updateOne(q, {
                $push: {
                    [key]: value
                }
            })
    }

    async pull(key, value, query) {

        let q = query || this.q

        return this.mongo.db.collection(this.collection)
            .updateOne(q, {
                $pull: { [key]: value}
            })
    }

}